print('############## UNIVERSIDAD NACIONAL DE LOJA #########################')
print('Ejercicio #4 de repaso de Lilia Susana Tene')
#Ejercicio 4: Asume que ejecutamos las siguientes sentencias de asignación:
ancho = 17
alto = 12
#Para cada una de las expresiones siguientes, escribe el valor de la expresión y el
#tipo (del valor de la expresión).
a = float(ancho/2)
b = float(ancho/2.0)
c = int(alto/3)
d = int(1 + 2 * 5)
print('Operación #1 ',a )
print('Operación #2 ',b )
print('Operación #3 ',c )
print('Operación #4 ',d )